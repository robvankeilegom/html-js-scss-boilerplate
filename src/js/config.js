window.config = {
  wheel: {
    radius: 440,
    angularVelocity: 10,
    outlineColor: { r: 255, g: 255, b: 255 },
    outlineWidth: 5,
  },
  pointer: {
    radius: 50,
  },
  logo: {
    radius: 150,
    color: { r: 180, g: 180, b: 180 },
  },
  pricePopup: {
    backgroundColor: { r: 255, g: 0, b: 255 },
    prefix: 'Jouw prijs is: ',
  },
  prices: [
    {
      name: 'GRABBELTON',
      icon: '\uf06b',
      occurrence: 10,
      iconColor: { r: 195, g: 150, b: 13 },
    },
    {
      name: 'DRAADLOZE MUISMAT',
      icon: '\uf245',
      occurrence: 2,
      iconColor: { r: 0, g: 104, b: 145 },
    },
    {
      name: 'SNOEPPOT',
      icon: '\uf786',
      occurrence: 3,
      iconColor: { r: 164, g: 9, b: 49 },
    },
    {
      name: 'GRATIS SOFTWARE-UPDATE',
      icon: '\uf6be',
      occurrence: 1,
      iconColor: { r: 79, g: 111, b: 25 },
    },
  ],
  colors: {
    tagor: { r: 164, g: 9, b: 49 },
    oras: { r: 0, g: 104, b: 145 },
    ordas: { r: 195, g: 150, b: 13 },
    ols: { r: 79, g: 111, b: 25 },
    highlight: { r: 0, g: 0, b: 0 },
    icon: { r: 255, g: 255, b: 255 },
  },
  icon: {
    fontSize: 70,
  }
}